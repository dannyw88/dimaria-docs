const babel = require('gulp-babel'),
    cssmin = require('gulp-cssmin'),
    del = require('del'),
    exec = require('gulp-exec'),
    gulp = require('gulp'),
    htmlhint = require('gulp-htmlhint'),
    htmlmin = require('gulp-htmlmin'),
    jade = require('gulp-jade'),
    less = require('gulp-less'),
    plumber = require('gulp-plumber'),
    requirejsOptimize = require('gulp-requirejs-optimize'),
    rename = require('gulp-rename');

gulp.task('html', () => {
    return gulp.src(['view/index.jade'])
        .pipe(plumber())
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('public'));
});

gulp.task('css', () => {
    del(['public/*.min.css', 'static/*.min.css']);
    return gulp.src('less/**/*.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public'));
});

gulp.task('js', () => {
    del(['public/*.min.js', 'static/*.min.js']);
    return gulp.src('js/index.js')
        .pipe(plumber())
        .pipe(babel({presets: ['es2015']}))
        .pipe(requirejsOptimize({
             include: [require.resolve("almond"), 'index'],
             wrap: true,
             preserveLicenseComments: false
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public'));
});

gulp.task('default', ['html', 'css', 'js']);

gulp.task('watch', () => {
    gulp.watch('view/**/*.jade', ['html']);
    gulp.watch('less/**/*.less', ['css']);
    gulp.watch('js/**/*.js', ['js']);
});
